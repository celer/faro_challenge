#include <iostream>
#include <sstream>

#include "RectLib/RectLoader.h"
#include "RectLib/RectPackerSystem.h"



int main ( int argc, char *argv[] )
{
    if (  argc < 2 )
    {
        std::cout << " No input File :" << std::endl;
        return 0;
    }

    std::stringstream ss =  std::stringstream(argv[1]);

    RectLoader rect_loader(ss.str());

    rect_loader.load();

    RectPackerSystem solver;

    std::vector<Rect> rect_packed;
    rect_packed.clear();

    auto area = solver.solve_rect_packing_problem(rect_loader.load(),rect_packed);

    std::cout << "Total Area :" << area << std::endl;  

    rect_loader.save(rect_packed);
    
    std::cin.get();

    return 0;
}


