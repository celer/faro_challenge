#ifndef _SHADER_MANAGER_HPP_
#define _SHADER_MANAGER_HPP_

//- Celer/OpenGL/ShaderManager.hpp - ShaderManager Module definition --------//
//- Celer Graphics
//  Copyrights (c) 2008-2019 - Felipe de Carvalho
//
//                    The Celer OpenGL Framework
//
// This file is distributed under GNU General Public License as published by
// the Free Software Foundation. @link http://www.gnu.org/licenses/gpl.html.
//
// @file
// @created on: Sep 26, 2012
// @version   : 0.1.0 Initial Release
// @brief This file contains the declaration of the ShaderManager class which
//        provides a wrapper to handle GLSL shader programs.
//
//---------------------------------------------------------------------------//


/// OpenGL Extensons 		- Need an OpenGL Context to use this class.
#include "../glad/glad.h"

#include <string>
#include <vector>
#include <map>

namespace Celer
{
        namespace OpenGL
        {
                class ShaderManager
                {                       
                        public:

                                struct Uniform
                                {
                                        GLint type;
                                        GLint location;
                                        GLint array_size;
                                        GLint array_stride;
                                        GLint offset;
                                };

                                /// @link http://www.opengl.org/wiki/Interface_Block_(GLSL)
                                /// If you are using CelerGL, make sure to translate the matrix after upload to GPU
                                /// The way that uniform block carrier data is complex.
                                /// This seems to work with a small blocks definition.
                                struct UniformBlock
                                {
                                                std::string 	               name;
                                                GLuint 		               index;
                                                GLuint		               size;
                                                std::map<std::string, Uniform> uniform;
                                };

                                struct SubRoutine
                                {
                                                // @brief SubRoutine belong to a shader stage.
                                                //        There is only one uniform location for them.
                                                //        One index for each subroutine.
                                                //
                                                GLenum 		shader_type;
                                                GLuint 		index;
                                                std::string 	name;

                                                GLuint 		uniform_location;
                                                std::string     uniform_name;
                                };


                                std::map<std::string, Uniform>                               uniforms_       = std::map<std::string, Uniform> ( );
                                std::map<std::string, UniformBlock>                          uniform_blocks_ = std::map<std::string, UniformBlock> ();
                                // FIXME <Uniform name, SubRoutine informations>
                                std::map<std::string, std::map <std::string , SubRoutine> > subroutines_    = std::map<std::string, std::map <std::string , SubRoutine> > ( );
                               
                                ShaderManager 	   ( std::string name = std::string("")  );
                                GLint    id	   ( );
                                
                                bool create 	   ( const std::string name , const std::string vertexShader , const std::string geometryShader , const std::string fragmentShader );

                                void addUniforms   ( );
                                void addUniform    ( std::string name , GLint type , GLint location, GLint array_size , GLint array_stride, GLint offset );

                                void addSubRoutines ( GLenum shader_type );
                                void addSubRoutine  ( std::string name , GLenum shader_type, GLint index, GLint uniform_location, std::string uniform_name );

                                void addUniformBlocks ( );

                                void active   	   ( );
                                void deactive 	   ( );

                                virtual ~ShaderManager ( );
                        private:
                                GLuint          id_        = GLuint(0);        ///< Identification
                                std::string     name_      = std::string("");      ///< Some name that remember the purpose of the shader.
                                GLint           isLinked_  = GLuint(0);  ///< Is it successfully complied                                
                                bool 	        compile 		( );
                                GLuint 	        loadShaderFromFile 	( GLenum shaderType , std::string fileName );
                                void 	        shaderLogInformation 	( GLuint id );
                                void 	        programLogInformation	( );
                };

        }

}

#endif /* _SHADER_MANAGER_HPP_ */

