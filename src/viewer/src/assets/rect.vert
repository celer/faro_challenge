#version 430 core

layout(location = 0) in vec4 position;

out VertexData
{
	vec4 vertice;
	vec4 color;
} VertexOut;

uniform mat4 ProjectionMatrix;

void main(void)
{

    VertexOut.vertice = vec4( position.xyz,1.0f);
    VertexOut.color = vec4 ( position.xyz , 1.0 );

    gl_Position = ProjectionMatrix * vec4 ( position.xyz , 1.0f );
}


