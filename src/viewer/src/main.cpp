#include <glad/glad.h>

#include <celer/shader_manager.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GLFW/glfw3.h>

#include <istream>
#include <sstream>

#include "RectLib/RectLoader.h"
#include "RectLib/RectPackerSystem.h"

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH =  800;
const unsigned int SCR_HEIGHT = 800;

GLfloat aspect_ration = 1.0f;

glm::mat4 projection_matrix = glm::mat4(1.0); 

std::vector<glm::vec4>  createRect( GLfloat x, GLfloat y, GLfloat width, GLfloat heigth)
{
    std::vector<glm::vec4> list_of_vertices = 
    {
        glm::vec4( x,  y,  0.0f, 0.0f), 
        glm::vec4( x + width, y, 0.0f, 0.0f), 

        glm::vec4( x, y + heigth, 0.0f, 0.0f), 
        glm::vec4( x + width, y + heigth, 0.0f, 0.0f)
    };

    return  list_of_vertices;
}


int main ( int argc, char *argv[] )
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Faro Test", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // print some gl infos (query)

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }    


    Celer::OpenGL::ShaderManager rect_shader_;

    rect_shader_.create("rect shader","assets/rect.vert","assets/rect.geom","assets/rect.frag");

    
  // Configure VAO/VBO
    GLuint VBO;
    GLuint quadVAO;
    std::vector<glm::vec4> vertex_data;

    // Help in fill the vertex data vector
    std::vector<std::vector<glm::vec4>> rects;
    // From packer algorithm
    std::vector<Rect> rect_packed;

    RectPackerSystem pack_system;

    if (  argc < 2 )
    {
        return 0;
    }

    std::stringstream ss =  std::stringstream(argv[1]);

    RectLoader rect_loader(ss.str());

    rect_loader.load();

    auto area = pack_system.solve_rect_packing_problem(rect_loader.load(), rect_packed);

    std::cout << "Total Area : " << area << std::endl;  

    rects.clear();
    rects.resize(rect_packed.size());
    

    for ( auto i = 0; i < rect_packed.size(); i++)
    {
   
        rects[i] = createRect(rect_packed[i].getOrigin().x, rect_packed[i].getOrigin().y, rect_packed[i].width(), rect_packed[i].height());
        
        vertex_data.insert(vertex_data.end(),rects[i].begin(),rects[i].end());
    }  

    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &VBO);
    
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertex_data.size() * sizeof(glm::vec4), vertex_data.data(), GL_STATIC_DRAW);

    glBindVertexArray(quadVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);  
    glBindVertexArray(0);


    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
    aspect_ration = static_cast<GLfloat>(SCR_WIDTH) / static_cast<GLfloat>(SCR_HEIGHT);
    projection_matrix = glm::ortho(0.0f, static_cast<GLfloat>(SCR_WIDTH)* aspect_ration, static_cast<GLfloat>(SCR_HEIGHT)* aspect_ration, 0.0f, -1.0f, 1.0f);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // input
        // -----
        processInput(window);

        glClear(GL_COLOR_BUFFER_BIT); 
        // render
        // ------    REd - GREEN - BLUE
        glClearColor(0.75f, 0.75f, 0.75f, 1.0f);

        rect_shader_.active();
        glUniformMatrix4fv(rect_shader_.uniforms_.at("ProjectionMatrix").location, 1, GL_FALSE, glm::value_ptr(projection_matrix));
        glUniform2f(rect_shader_.uniforms_.at("view_port").location,static_cast<GLfloat>(SCR_WIDTH*aspect_ration),static_cast<GLfloat>(SCR_HEIGHT*aspect_ration));
            glBindVertexArray(quadVAO);
                glDrawArrays(GL_LINES_ADJACENCY, 0, vertex_data.size());
            glBindVertexArray(0);
        rect_shader_.deactive();
        
        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();

    rect_loader.save(rect_packed);

    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);

    aspect_ration = static_cast<GLfloat>(width)/static_cast<GLfloat>(height);

    projection_matrix =  glm::ortho(0.0f, static_cast<GLfloat>(width)*aspect_ration, static_cast<GLfloat>(height)*aspect_ration, 0.0f, -1.0f, 1.0f); 
}

