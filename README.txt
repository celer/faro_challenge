=======
## Rect Packed Solution

There are two projects: 

#"cmd", a simples command line app. Just plain C++
 - To use it, just pass an .txt file with rect data in the format: "number number". Two number sparetad by space per line.
 - The total are of the solution is printed on the terminal and a text file named: "rect_packed_(current_time).txt" 
   with the coordinate of each rectangle is saved.
	
#"viewer", a simple OpenGL app. All the dependencies for the viewer are include, named:
 - Same as the cmd app
 - After load, the arrangement of the solution will be displayed using OpenGL. 

All dependencies for the viewer are included and linked statically. 
 - GLFW  - for the creation of the OpenGL context
 - glad  - for load OpenGL extension features
 - glm   - simple math library
 - celer - simple OpenGL wrapper create by me

This project uses CMake to generate the Visual Studio Solution. It is feree and can download here: https://cmake.org/
In order to generate a Visual Studio Solution using the follwing command line in a terminal:

#For visual studio 2017:

mkdir build
cd build
cmake -G "Visual Studio 15 2017" ..

#For visual studio 2019:

mkdir build
cd build
cmake -G "Visual Studio 16 2019" ..

The output binary are place at build/build/faro_challenge and build/build/faro_challenge_viewer

One step need to be done in th project "viewer"
Please, move the folder "assets" to where the binary resides, thus, the app can load the shaders.
Visual Solution creates an extra folder based on the build type, for instance: Debug/Release






 


>>>>>>> 6fb36038676021d1ddc0f7a36d5295e98f1a490c
