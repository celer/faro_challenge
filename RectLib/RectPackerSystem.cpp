    #include "RectPackerSystem.h" 
    
    #include <map>
    #include <limits>
    #include <algorithm>
    
    RectPackerSystem::RectPackerSystem( )
    {
        // Insert the first anchor point at the origin
        this->anchor_points.insert(Point2d(0,0));
        // Initial bounding box with maximu area
        square_bounding_box_ = Rect(0,0,std::numeric_limits<int>::max(),std::numeric_limits<int>::max());
    }

    // Solve the packing problem and return the area o the square bouding box
    int RectPackerSystem::solve_rect_packing_problem( const std::vector<Rect>& _rects, std::vector<Rect>& _rects_packed )
    {

    if ( _rects.empty() )
    {
        std::cout << "_rects.empty()" << std::endl;
        return 0;
    }

    this->rects_.clear();
    anchor_points.clear();
    _rects_packed.clear();
    this->square_bounding_box_ =  Rect();

    // At least one rect is available
    for (const auto& rect: _rects)
    {
        this->rects_.insert(rect);                  
    }       
    
    // grab the bigest
    auto current_rect = this->rects_.begin();

    // for ( auto a : this->rects_)
    // {
    //     std::cout << a.area() << std::endl;
    // }
    
    // create the initial bouding box
    this->square_bounding_box_ = Rect(0,0,current_rect->width(),current_rect->height());

    // initial anchor points
    anchor_points.insert(Point2d(current_rect->width(),0));
    anchor_points.insert(Point2d(current_rect->width(),current_rect->height()));
    anchor_points.insert(Point2d(0,current_rect->height()));

    // first rectangle
    _rects_packed.push_back(Rect(0,0,current_rect->width(),current_rect->height()));

    // take it off the set
    this->rects_.erase(current_rect);

    // the impact in the size of the bounding box
    int current_area = this->square_bounding_box_.area();
    std::set<Point2d> used_anchor_point;
    // avoid used anchor point to be alocate again
    used_anchor_point.insert(Point2d(0,0));
    
    // available places inside the container
    std::vector<std::pair<Point2d,int>> area_inside;
    // available places outise the container
    std::vector<std::pair<Point2d,int>> area_outside;

    while ( !this->rects_.empty() )
    {
        current_rect  = this->rects_.begin();    

        std::vector<std::pair<Point2d,int>> areas;
        area_inside.clear();
        area_outside.clear();

        // grab all possible anchor points    
        for ( const auto& ap : anchor_points )
        {
            auto bigest_side = (current_rect->width()+ap.x);
            
            if ( bigest_side < (current_rect->height()+ap.y) )
            {
                bigest_side = (current_rect->height()+ap.y);
            }

            auto area = bigest_side * bigest_side;

            if ( area >= this->square_bounding_box_.area())
            {
                area_outside.push_back({Point2d(ap.x,ap.y), area });
            }else
            {
                area_inside.push_back({Point2d(ap.x,ap.y), area });
            }                             
        }
        
        // if false, it didnt fit inside
        bool packed = false;

        for ( auto inside_pos: area_inside )
        {
            Rect new_rect = Rect(inside_pos.first.x,inside_pos.first.y,current_rect->width(),current_rect->height());

            bool valid_anchor = true;    

            for( const auto& r : _rects_packed)           
            {
                if (new_rect.is_overlap(r))
                {
                    if ( new_rect.is_hooked( r ) )
                    {
                        continue;
                    }else
                    {
                        valid_anchor = false;
                        break;
                    }
                    
                }
            }


            if (valid_anchor)
            {
                auto pos = inside_pos.first;

                used_anchor_point.insert(Point2d(pos.x,pos.y));

                auto p = Point2d(pos.x + current_rect->width(), pos.y);
                if ( used_anchor_point.find(p) == used_anchor_point.end())     
                {
                    anchor_points.insert(p);
                }
                p = Point2d(pos.x + current_rect->width(), pos.y + current_rect->height());
                if ( used_anchor_point.find(p) == used_anchor_point.end())     
                {
                    anchor_points.insert(p);
                }
                p = Point2d(pos.x, pos.y + current_rect->height());
                if ( used_anchor_point.find(p) == used_anchor_point.end())     
                {
                    anchor_points.insert(p);
                }
                        
                auto d = (current_rect->width()+pos.x);
                    
                if ( d < (current_rect->height()+pos.y) )
                {
                        d = (current_rect->height()+pos.y);
                }

                if ( (d*d) > this->square_bounding_box_.area() )
                {
                    // create the initial bouding box
                    this->square_bounding_box_ = Rect(0,0,d,d);
                }

                _rects_packed.push_back(new_rect);

                anchor_points.erase(pos);

                packed = true;

                break;

            }else
            {
               continue;
            }
        }

        if ( !packed )
        {
            std::sort(area_outside.begin(), area_outside.end(), 
            [](const std::pair<Point2d,int> & a, const std::pair<Point2d,int> & b) -> bool
            {   
                // decrease order of area
                return a.second > b.second; 
            });

            for ( auto outside_pos: area_outside )
            {
                Rect new_rect = Rect(outside_pos.first.x,outside_pos.first.y,current_rect->width(),current_rect->height());

                bool valid_anchor = true;    

                for( const auto& r : _rects_packed)           
                {
                    if (new_rect.is_overlap(r))
                    {
                        if ( new_rect.is_hooked( r ) )
                        {
                            continue;
                        }else
                        {
                            valid_anchor = false;
                            break;
                        }
                        
                    }
                }

                if (valid_anchor)
                {
                    auto pos = outside_pos.first;

                    used_anchor_point.insert(Point2d(pos.x,pos.y));

                    auto p = Point2d(pos.x + current_rect->width(), pos.y);
                    if ( used_anchor_point.find(p) == used_anchor_point.end())     
                    {
                        anchor_points.insert(p);
                    }
                    p = Point2d(pos.x + current_rect->width(), pos.y + current_rect->height());
                    if ( used_anchor_point.find(p) == used_anchor_point.end())     
                    {
                        anchor_points.insert(p);
                    }
                    p = Point2d(pos.x, pos.y + current_rect->height());
                    if ( used_anchor_point.find(p) == used_anchor_point.end())     
                    {
                        anchor_points.insert(p);
                    }
                            
                    auto d = (current_rect->width()+pos.x);
                        
                    if ( d < (current_rect->height()+pos.y) )
                    {
                            d = (current_rect->height()+pos.y);
                    }

                    if ( (d*d) > this->square_bounding_box_.area() )
                    {
                        // create the initial bouding box
                        this->square_bounding_box_ = Rect(0,0,d,d);
                    }

                    _rects_packed.push_back(new_rect);

                    anchor_points.erase(pos);

                    packed = true;                    

                    break;

                }else
                {
                    continue;
                }
            }                          

        }
        
        this->rects_.erase(current_rect);

    }  // end while


    return (this->square_bounding_box_.area());
}    


