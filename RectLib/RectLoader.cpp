#include <fstream>
#include <iostream>

#include <sstream>
#include <chrono>
#include <iomanip>

#include "RectLoader.h"

RectLoader::RectLoader( ) : file_path_("")
{

}

RectLoader::RectLoader(const std::string& filePath) : file_path_(filePath)
{
    
}

void RectLoader::printRects( const std::vector<Rect>& _list_of_rects)
{
    for (const auto& rect: _list_of_rects)
    {
        std::cout << "widht: " <<  rect.width() << " | height: " << rect.height() << std::endl ;
    }
}

std::vector<Rect> RectLoader::load()
{
    std::vector<Rect> list_of_rects = {};

    std::ifstream infile;
    infile.open (this->file_path_, std::ifstream::in);

    if (infile.fail()) {
        std::cerr << "Error opeing the rect file" << std::endl;
        infile.close();
        exit(1);
    } 

    int width  = 0;
    int height = 0;

    while (infile >> width >> height) 
    {     
        list_of_rects.push_back(Rect(width,height));
    }
    
    infile.close();

    //printRects(list_of_rects);

    return list_of_rects;
    
}


void RectLoader::save(const std::vector<Rect>& _rects)
{
    // Save result rectangle with coodinates to a file in the format: "x y widht height"
    const auto time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::stringstream str_time;

    str_time << std::put_time(std::localtime(&time), "%H_%M_%S");
    std::string filename = "result_packed_" + str_time.str() + ".txt";
    std::ofstream result_packed_file(filename, std::ios::app);

    if (result_packed_file.fail())
    {
        std::cout << " Fail to create :" << filename << std::endl;
    }

    for (const auto& r : _rects)
    {
        result_packed_file << r.getOrigin().x << " " << r.getOrigin().y << " " << r.width() << " " << r.height() << "\n";
    }

    result_packed_file.close();
}