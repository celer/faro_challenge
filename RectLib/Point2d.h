#ifndef _POINT2D_HPP_
#define _POINT2D_HPP_

class Point2d
{
    public:

        int x = 0;
        int y = 0;

        Point2d            ( ) = default; 
        Point2d            ( int _x, int _y );
               
        // Copy constructors
        Point2d            (const Point2d& _another_ppint ) = default;
        Point2d& operator= (const Point2d& _another_point ) = default;

        // Move construtors
        Point2d            ( Point2d&& _another_point ) noexcept = default ;
        Point2d& operator= ( Point2d&& _another_point ) noexcept = default ;                        

        // Sort and store point
        bool operator<     (const Point2d& _another_point) const;                    
        bool operator==    (const Point2d& _another_point) const;
                
};

#endif /* Point2D */