#ifndef _RECT_LOADER_HPP_
#define _RECT_LOADER_HPP_


#include <string> 
#include <vector>

#include "Rect.h"

class RectLoader
{
    public:
        RectLoader             ( );
        RectLoader             (const std::string& filePath);
        
        // Load rect from a file:  "x y" per line
        std::vector<Rect> load ( );

        // save a list of rect to a file: "x y width height" per line
        void save( const std::vector<Rect>& _rects);
        
        RectLoader             (const Rect& _another_rectlaoder) = delete;
        RectLoader& operator=  (const RectLoader& _another_rectlaoder) = delete;       

        // Debug functions
        void printRects        (const std::vector<Rect>& _list_of_rects);
    private:
        std::string file_path_;
};

#endif /* _RECT_LOADER_HPP_ */