#include <cmath>
#include <iostream>
#include <algorithm>

#include "Rect.h"


Rect::Rect( int _width, int _height ) : origin_(0, 0),
                                        width_ (_width), 
                                        height_(_height),
                                        area_(_width * _height),
                                        aspect_ration_(_width/_height)                                
{
    
}

Rect::Rect( int _x, int _y, int _width, int _height ) : origin_(_x,_y),
                                                        width_ (_width), 
                                                        height_(_height),
                                                        area_(_width * _height),
                                                        aspect_ration_(_width/_height)                                
{

}

// sort by area
bool Rect::operator<  ( const Rect& _another_rect ) const
{
    return ( this->area() > _another_rect.area() );
}

int Rect::width() const
{
    return this->width_;
}

int Rect::height() const
{
    return this->height_;
}

int  Rect::area() const
{
    return this->area_;
}

float Rect::aspect_ration () const
{
    return this->aspect_ration_;
}

Point2d Rect::getOrigin( ) const
{
    return this->origin_;
}

void Rect::setOrigin ( Point2d _new_origin )
{
    this->origin_ =_new_origin;
}

bool Rect::is_overlap( const Rect& _another_rect ) const
{
    auto ax1 = this->getOrigin().x;
    auto ax2 = this->getOrigin().x + this->width();
    auto ay1 = this->getOrigin().y;
    auto ay2 = this->getOrigin().y + this->height();

    auto bx1 = _another_rect.getOrigin().x;
    auto bx2 = _another_rect.getOrigin().x + _another_rect.width();
    auto by1 = _another_rect.getOrigin().y;
    auto by2 = _another_rect.getOrigin().y + _another_rect.height();

    bool noOverlap  = 
     ( 
        ( (ax1) > (bx2) )  ||
        ( (bx1) > (ax2) )  ||
        ( (ay1) > (by2) )  ||
        ( (by1) > (ay2) )  
    ); 
    
    return !noOverlap;
   
}

/// touch the rectangle but is not inside
bool Rect::is_hooked( const Rect& _rect ) const
{
    auto p1r = Point2d( this->getOrigin().x, this->getOrigin().y);
    auto p2r = Point2d( this->getOrigin().x + this->width(), this->getOrigin().y);
    auto p3r = Point2d( this->getOrigin().x +this->width(), this->getOrigin().y + this->height());
    auto p4r = Point2d( this->getOrigin().x, this->getOrigin().y + this->height());
    
    std::vector<Point2d> points = { Point2d(_rect.getOrigin().x,  _rect.getOrigin().y),
                                    Point2d(_rect.getOrigin().x + _rect.width(),_rect.getOrigin().y ), 
                                    Point2d(_rect.getOrigin().x + _rect.width(),_rect.getOrigin().y + _rect.height()),
                                    Point2d(_rect.getOrigin().x, _rect.getOrigin().y + _rect.height() ) };



    bool point_inside = false;

    for ( const auto& p: {p1r,p2r,p3r,p4r} )        
    {
        if ( (p.x > _rect.getOrigin().x)                    &&
             (p.x < (_rect.getOrigin().x + _rect.width() ) )  &&
             (p.y > _rect.getOrigin().y)                    &&
             (p.y < (_rect.getOrigin().y + _rect.height() ) ) 
            )
        
        {
            point_inside = true;
            break;   
        }
             
    }

    bool has_shared_corner = false;

    for ( const auto& p : points)
    {
        if ( (p == p1r) || (p == p2r) || (p == p3r) || (p == p4r) )   
        {
            has_shared_corner = true;
            break;
        }
    } 

    return ( !point_inside && has_shared_corner );
}
