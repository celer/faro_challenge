#ifndef _RECT_HPP_
#define _RECT_HPP_

#include <vector>

#include "Point2d.h"

// Class to hold rectangle data and some useful function to aid 
// in the packing algorithm 
class Rect
{
    public:               

        Rect            ( int _with, int _height );                               
        Rect            ( int _x, int _y, int _with, int _height );
        Rect            ( ) = default; 
               
        // Copy constructors
        Rect            (const Rect& _another_rect ) = default;
        Rect& operator= (const Rect& _another_rect ) = default;

        // Move construtors
        Rect            ( Rect&& _another_rect ) noexcept = default ;
        Rect& operator= ( Rect&& _another_rect ) noexcept = default ;               
        
        // sort by area
        bool operator<  ( const Rect& _another_rect ) const;   

       ~Rect() = default;    

        int     width         ( ) const;
        int     height        ( ) const;
        int     area          ( ) const;     
        float   aspect_ration ( ) const; 
        Point2d getOrigin     ( ) const;
        void    setOrigin     ( Point2d _new_origin );

        
        bool is_overlap ( const Rect& _another_rect ) const;

        // Test if the rectacle is overlaped just by edge or point
        bool is_hooked( const Rect& _rect ) const;

    private:

        // Clockwise orientation      
        //                   
        //    (x,y)       (x+width,y)
        //      1             2
        //      +-------------+
        //      |             |
        //      |             |
        //      +-------------+  
        //      4             3 
        //  (x,y+height) (x+width,y+height) 

        // Origin of the Rect
        Point2d origin_      = Point2d(0,0);
        // unitary cube as default rectangle;
        int   width_         = 1;
        int   height_        = 1;
        // space coordinate 
        float aspect_ration_ = 1;
        int   area_          = 1;
       
};


#endif /* _RECT_HPP_ */