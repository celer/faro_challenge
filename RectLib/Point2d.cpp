#include "Point2d.h"

Point2d::Point2d( int _x, int _y ) : x(_x), y(_y)
{
    
}

bool Point2d::operator<(const Point2d& _another_point) const
{
    return (x != _another_point.x ? x < _another_point.x : y < _another_point.y );
}

bool Point2d::operator==(const Point2d& _another_point) const
{
    return ( (x == _another_point.x) && (y == _another_point.y) );
}

