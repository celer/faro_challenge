#ifndef _RECT_PACKER_SYSTEM_HPP_
#define _RECT_PACKER_SYSTEM_HPP_


#include <iostream>
#include <set>
#include <vector>

#include "Rect.h"


// Used to solve the rect packing problem
class RectPackerSystem
{
    public:

      RectPackerSystem();   
     ~RectPackerSystem() =  default;


      // Solve the packing problem and return the area o the square bouding box
      // The arae of the result square is : square_bounding_box_.x * square_bounding_box_.y
      int solve_rect_packing_problem( const std::vector<Rect>& _rects, std::vector<Rect>& _rects_packed );

      private:
       
         // list of avaible anchor points where a rect can be placed
         std::set<Point2d> anchor_points;
              // Extreme point of the squared boudingbox
         Rect square_bounding_box_;

        // List of rects sorted by area
        //  +-------+
        //  |       |
        //  +-------+         
        std::multiset<Rect> rects_;

        void addRect(const Rect& r );

};


#endif /* _RECT_PACKER_SYSTEM_HPP_ */